# OpenML dataset: fars

https://www.openml.org/d/40672

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Re-upload of the dataset as it is present in the Penn ML Benchmark (https://github.com/EpistasisLab/penn-ml-benchmarks/tree/master/datasets/classification/fars).
It's a dataset on traffic accidents, see https://data.world/nhtsa/fars-data.
I am not sure of the specific date or aggregation method as it is just a re-upload.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40672) of an [OpenML dataset](https://www.openml.org/d/40672). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40672/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40672/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40672/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

